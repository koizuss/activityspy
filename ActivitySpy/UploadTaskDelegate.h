//
//  UploadTaskDelegate.h
//  ActivitySpy
//
//  Created by 進藤 孝司 on 2014/02/22.
//  Copyright (c) 2014年 進藤 孝司. All rights reserved.
//

#import <Foundation/Foundation.h>

// NSURLSessionTaskDelegateプロトコルの実装クラスです
@interface UploadTaskDelegate : NSObject <NSURLSessionTaskDelegate>

// URLSession:task:didCompleteWithError:
// Tells the delegate that the task finished transferring data.
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error;

// URLSession:task:didReceiveChallenge:completionHandler:
// Requests credentials from the delegate in response to an authentication request from the remote server.
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential *credential))completionHandler;

// URLSession:task:didSendBodyData:totalBytesSent:totalBytesExpectedToSend:
// Periodically informs the delegate of the progress of sending body content to the server.
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didSendBodyData:(int64_t)bytesSent totalBytesSent:(int64_t)totalBytesSent totalBytesExpectedToSend:(int64_t)totalBytesExpectedToSend;

// URLSession:task:needNewBodyStream:
// Tells the delegate when a task requires a new request body stream to send to the remote server.
// Note: You do not need to implement this if your code provides the request body using a file URL or an NSData object.
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task needNewBodyStream:(void (^)(NSInputStream *bodyStream))completionHandler;

// URLSession:task:willPerformHTTPRedirection:newRequest:completionHandler:
// Tells the delegate that the remote server requested an HTTP redirect.
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task willPerformHTTPRedirection:(NSHTTPURLResponse *)response newRequest:(NSURLRequest *)request completionHandler:(void (^)(NSURLRequest *))completionHandler;

@end
