//
//  UploadTaskDelegate.m
//  ActivitySpy
//
//  Created by 進藤 孝司 on 2014/02/22.
//  Copyright (c) 2014年 進藤 孝司. All rights reserved.
//

#import "UploadTaskDelegate.h"

@implementation UploadTaskDelegate{
    NSString *filePath;
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error
{
    if(error){
        
        NSLog(@"%ld:%@", (long)[error code],[error localizedDescription]);
        
    }else{
        NSString *alertAction = @"Upload succeeded!";
        NSString *alertBody = @"プロセス情報のアップロードに成功しました。";
        [self notifyWithAction:alertAction body:alertBody];
    }
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential *credential))completionHandler
{
    NSLog(@"%@", @"didReceiveChallenge");
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didSendBodyData:(int64_t)bytesSent totalBytesSent:(int64_t)totalBytesSent totalBytesExpectedToSend:(int64_t)totalBytesExpectedToSend
{
    NSLog(@"bytesSent:%lld, totalBytesSent:%lld, totalBytesExpectedToSend:%lld", bytesSent, totalBytesSent, totalBytesExpectedToSend);
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task needNewBodyStream:(void (^)(NSInputStream *bodyStream))completionHandler
{
    NSLog(@"%@", @"needNewBodyStream");
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task willPerformHTTPRedirection:(NSHTTPURLResponse *)response newRequest:(NSURLRequest *)request completionHandler:(void (^)(NSURLRequest *))completionHandler
{
    NSLog(@"%@", @"willPerformHTTPRedirection");
    completionHandler(request);
}

- (void)notifyWithAction:(NSString*)action body:(NSString*)body
{
    [[UIApplication sharedApplication] cancelAllLocalNotifications];

    UILocalNotification *localNotification = [[UILocalNotification alloc]init];
    localNotification.alertAction = action;
    localNotification.alertBody = body;
    localNotification.fireDate = [NSDate date];
    localNotification.timeZone = [NSTimeZone localTimeZone];
    localNotification.soundName = UILocalNotificationDefaultSoundName;
    
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
}

@end
