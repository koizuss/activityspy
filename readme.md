# ActivitySpy

iOSのバックグラウンドで不定期に動作します。  
起動しているプロセスの情報をサーバーへ送信します。

## Requirement
iOS 7.0 以上

## Note

### urlSession.plist の項目を任意のものに書き換えてください

* URL（送信先サーバーのURL）
* PARAM_NAME（パラメータ名）
* BOUNDARY（バウンダリ）
* UPLOAD_FILE（アップロードファイル名）
* NSURLSESSION_IDENTIFIER（NSURLSessionオブジェクトの識別子）

### プロジェクト設定でBackground fetchを有効にしてください
プロジェクト設定->Capabilities->Background Modes の Background fetchを有効にする。
![プロジェクト設定](https://bitbucket.org/shindooo/activityspy/downloads/project_settings.png)

###iOS端末の設定で「Appのバックグラウンド更新」をOFFにしないでください
設定->一般->Appのバックグラウンド更新
![iOS設定](https://bitbucket.org/shindooo/activityspy/downloads/iOS_preferences.png)
